TARGET = pcre
include($$PWD/../../commonplatform.pri)
CONFIG += staticlib

DEFINES += HAVE_CONFIG_H

win32 {
INCLUDEPATH += 8.31/windows
}

linux|integrity {
INCLUDEPATH += 8.31/linux
}

mac {
INCLUDEPATH += 8.31/macosx
}

INCLUDEPATH += 8.31

clang {
#Ignore selected warnings from pcre lib (as it's a 3rd party lib)
QMAKE_CXXFLAGS_WARN_ON = -Wall \
    -Wno-unused-parameter \
    -Wno-missing-field-initializers \
    -Wno-unused-variable

QMAKE_CFLAGS_WARN_ON = -Wall \
    -Wno-unused-parameter \
    -Wno-missing-field-initializers \
    -Wno-unused-variable
}

include(pcre.pri)

load(qt_helper_lib)
